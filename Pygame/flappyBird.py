import pygame
from pygame.locals import*
import random
import time as t

red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
white = (255,255,255)
yellow = (255,255,0)
black = (0,0,0)
xBird = 315
yBird = 240
yChange = 40
pipeX = 480
pipeY = random.randint(-270,0)
pipeHeight = 320
gap = 150
score = 0

bird = []

pygame.init()

font = pygame.font.SysFont(None,25)

screen = pygame.display.set_mode((640,480))
clock = pygame.time.Clock()

def MSG(msg, color, x, y):
    screen_text = font.render(msg,True, color)
    screen.blit(screen_text,[x,y])
    
backgroundDay = pygame.image.load("./images/background-day.png")
backgroundDay  = pygame.transform.scale(backgroundDay,(640,480))

pipeBottom = pygame.image.load("./images/pipe-green.png")
pipeTop = pygame.image.load("./images/pipe-green.png")
pipeTop = pygame.transform.flip(pipeTop, 0, 1)

midBird = pygame.image.load("./images/yellowbird-midflap.png")
                            
upBird = pygame.image.load("./images/yellowbird-upflap.png")

downBird = pygame.image.load("./images/yellowbird-downflap.png")

base = pygame.image.load('./images/base.png')
base = pygame.transform.scale(base, (640,50))

bird =[midBird,upBird,downBird]
count = 0
flap = 0

fps = 10

while True:
    screen.fill(white)
    screen.blit(backgroundDay,(0,0))

    yBird = yBird + 5
    pipeX = pipeX - 5
    clock.tick(fps)
    
    #pygame.draw.circle(screen, yellow, (xBird, yBird),15)
    screen.blit(bird[count],(xBird - 15,yBird - 15))
    screen.blit(pipeTop,(pipeX,0))
    #screen.blit(pipeBottom,(pipeX, pipeHeight + gap))
    screen.blit(base, (0,430))
    
    if pipeX < 0:
        pipeX = 600   
        pipeY = random.randint(50,270)
        score = score + 1
        fps = fps + 5
        pipeTop = pygame.transform.scale(pipeTop, (50, pipeHeight))

    MSG(str(score), black,0,0)

    if score == 10: 
        MSG("YOU WIN!!!!! YOU DID IT 10 TIMES", green, 320,240)

    if xBird +15==pipeX and  0< yBird - 15 < pipeHeight:
        print('collision')
        MSG("HIT", black,320,240)
        break

    if flap == 1:
        count = count + 1
        if count == 3:
            count = 0
            flap = 0
            
    if xBird +15==pipeX and  (pipeHeight + gap)< yBird - 15 < 430:
        print ('boom')
        MSG('HIT', black,320,240)
        break
     #   print('collision')

    if yBird - 15 == 0:
        print ('top hit')
        MSG('TOP HIT', black,320,240)
        break

    if yBird + 15 == 430:
        print ('bottom hit')
        MSG('BOTTOM HIT', black,320, 240)
        break

    
    pygame.display.update()

    for event in pygame.event.get():
        
        if event.type==KEYDOWN:      #  hit a key
            if event.key==K_SPACE:
                flap = 1
                yBird=yBird-yChange      # ychange is the change in value of y
        
        if event.type == QUIT:
            pygame.quit()
            exit()
        

print('GAME OVER')
pygame.display.update()
