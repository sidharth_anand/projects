import random

import pygame
from pygame.locals import *

red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
white = (255,255,255)
black = (0,0,0)
screen = pygame.display.set_mode((640, 480))

foodx = (random.randint(0, 640)//10)*10
foody = (random.randint(0, 480)//10)*10

snakeX = (random.randint(0, 640)//10)*10
snakeY = (random.randint(0, 480)//10)*10

#font = pygame.font.SysFont(None, 50)

def MSG(msg, color, x, y):
    screen_text = font.render(msg,True, color)
    screen.blit(screen_text,[x,y])

score = 0
top = 0
bottom = 0
leftSide = 0
rightSide = 0

down = 0
up = 0
left = 0
right = 0

speed = 5

clock = pygame.time.Clock()

#For adding multiple body parts
snakelist = []
snakelist.insert(0,(snakeX,snakeY))

while True:
    screen.fill(black)
    snakelist.pop()
    snakelist.insert(0,(snakeX,snakeY))
   # print(snakelist)
    
    for x in snakelist:
        pygame.draw.rect(screen, green, (x[0], x[1], 10, 10))
        
    # drawing food and snake
    #pygame.draw.rect(screen,green,(snakex, snakey , 10, 10))
    pygame.draw.rect(screen,red,(foodx, foody , 10, 10))

    #t.sleep(0.1)
    clock.tick(speed)
    if snakeX == foodx and snakeY == foody:
        #When food is eaten, the food is transported to a random location
        print("Snake ate the food")
        score += 1
        
        speed += 2.5
        
        snakelist.append((snakeX,snakeY))
        
        #MSG(score, white, 0, 0)
        foodx = (random.randint(0, 640)//10)*10
        foody = (random.randint(0, 480)//10)*10

    if snakeX < 0:
        snakeX  = 630

    if snakeX>630:
        snakeX = 0

    if snakeY < 0:
        snakeY = 470

    if snakeY > 470:
        snakeY = 10

    pygame.display.update()
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit()

        # This is where all of the snake movement happens
        # When  speific key is pressed, a variable is set to one and all the others are set to 0
        # This causes the movement to happen over and over again.
        elif event.type == pygame.KEYDOWN:
            if event.key == K_DOWN and up != 1:
                down = 1
                up = 0
                right = 0
                left = 0
                
            if event.key == pygame.K_LEFT and right != 1:
                left = 1
                right = 0
                up = 0
                down = 0
            if event.key == pygame.K_RIGHT and left != 1:
                right = 1
                down = 0
                up = 0
                left = 0
            if event.key == pygame.K_UP and down != 1:
                up = 1
                down = 0
                right = 0
                left = 0 

# Snake movement happens here
    if down ==1:
        snakeY += 10
    if up == 1:
        snakeY -= 10
    if right == 1:
        snakeX += 10
    if left == 1:
        snakeX -= 10

'''
    if snakex -10 == 0:
        leftside = 1
        rightside = 0
        bottom = 0
        top = 0
    
    if snakex + 10 == 640:
        leftside = 0
        rightside = 1
        bottom = 0
        top = 0
        
    #if snakey + 10 == 480:
     #   print('wall hit')
      #  snakey = 0
    #if snakey -10 == 0:
     #   print ('wall hit')
      #  snakey = 480

    if leftside == 1:
        print ('wallhit')
        snakex = 640
        leftside = 0
    if rightside == 1:
        snakex = 0
        rightside = 0
'''
