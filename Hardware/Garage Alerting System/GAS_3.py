import RPi.GPIO as GPIO
import time as t
import smtplib

        
echo = 12
trig = 25
counter = 0
maxs = 15
distance = 0
#Setting up the vairables

GPIO.setmode(GPIO.BCM)
GPIO.setup(echo,GPIO.IN)
GPIO.setup(trig,GPIO.OUT)
GPIO.setup(21,GPIO.OUT)
GPIO.setup(20,GPIO.IN)
#This is setting up the pins and setting up the mode

#This is setting up the Email
def sendemail(subject,sender,destination,Text,password):
    server = smtplib.SMTP('mail.sidharthanand.com', 26)
    server.starttls()
    server.login(sender,password)
    server.ehlo()
    #^^^ is setting up where and how the email be set
    BODY = '\r\n'.join(['To: %s' % destination,
                        'From: %s' % sender,
                        'Subject: %s' %subject,
                        Text])
    #^^^ This is setting up the format for the email
    try:
        server.sendmail(sender,[destination],BODY)
        print('Email Sent')
    except:
        print("Error sending mail")                                         
    server.quit()    
    #If it works the email will be sent and if it fails it will fail sending the email

def utscs ():
    GPIO.output(trig,0)
    t.sleep(0.5)

    GPIO.output(trig,1)
    t.sleep(0.00001)
    GPIO.output(trig,0)

    start = t.time()
    while GPIO.input(echo)== 0:
         start = t.time()

    stop = t.time()
    while GPIO.input(echo) == 1:
        stop = t.time()
    #The ultrasonic senesor send out 2 beams of sound
    #based of the timing differences the distance in centimeters may be
    #calculated
    elap = stop - start
    distance = elap*34000
    distance = distance/2
    return(distance)
    #All of the above is calculating the distance 
while True:
    try:
        c = GPIO.input(20)
        distance = utscs()
        print(distance,c)
        print(c)
        #This is setting up the ultrasonice sensor and this is detecting if the garage is closed
        if c ==0:
            print("Door Closed.")
            #This is showing that the garage is closed
        else:
            t.sleep(10)
            if c== 1 and distance >= 6  and counter < maxs :
                counter= counter + 1
                sendemail('YOUR GARAGE IS OPEN','your email here','phone number email',
                          ' Your garage is open. Please close your garage.', 'password to email')
                # This is where the email is being sent and it is calculating if the garage is open
            else:
                print('Door Closed')
                counter = 0
                # This is reseting the email counter and calculatin if the garage is closed
    except KeyboardInterrupt:
        GPIO.cleanup()
        exit()
        #This is the cleaning up of the code and the exiting the program

