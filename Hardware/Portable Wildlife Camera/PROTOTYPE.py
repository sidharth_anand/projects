from picamera import PiCamera
from time import time,sleep
from os import system
from subprocess import call
import RPi.GPIO as GPIO
from datetime import datetime
import pygame

camera = PiCamera()
start= time()
camera.resolution=(1280,720)
PIR = 4
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR,GPIO.IN)
piccount=0
pictotal=10
try:
    while True:
        currenttime=datetime.now()
        pictime= currenttime.strftime('%Y.%m.%d:%H %M %S')
        name =pictime+'.jpg'
        filepath = '/home/pi/ywSidharthAnand/PWC/Science_Fair/codetest/'+name
        now = time()
        
        if GPIO.input(PIR) == 1:
            system('arecord -D hw:1,0 -d 10 -f S16_LE -r 44100 '+pictime+'.wav')
            for x in range(5): 
                currenttime=datetime.now()
                pictime= currenttime.strftime('%Y.%m.%d:%H %M %S')
                name =pictime+'.jpg'
                filepath = '/home/pi/ywSidharthAnand/PWC/Science_Fair/codetest/'+name
                camera.annotate_text=pictime
                camera.capture(filepath)
                sleep(2)
                print('image taken')
            
            camera.start_recording(name +' video.h264')
            print('recording video')
            sleep(10)
            camera.stop_recording()
            print('done recording')
            piccount = piccount +1
        if now - start >=3:
            camera.capture(filepath+'image{0:04d}.jpg')

except KeyboardInterrupt:
    print('Keyboard interrupted')

finally:
    print('generating timelapse')
    system('avconv -y -r 10 -i image%4d.JPG -r 10 -vcodec libx264 -q:v 20 -vf crop=4256:2832,scale=iw/4:ih/4 timelapse.mp4')
        
        
