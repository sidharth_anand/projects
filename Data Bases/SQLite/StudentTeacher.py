import sqlite3

conn=sqlite3.connect("YWClasses.db")

c=conn.cursor()
#c.execute("CREATE TABLE Students (StudentID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,StudentName NVARCHAR(255),ParentName NVARCHAR(255),City  NVARCHAR(255),Country NVARCHAR(255) , GPA FLOAT(4,2), dateStarted DATE)" )
#c.execute("CREATE TABLE Teachers (TeacherID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,TeacherName NVARCHAR(255),Phone NVARCHAR(255),city NVARCHAR(255),country NVARCHAR(255) )")
#c.execute("CREATE TABLE Classes (ClassID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,ClassName NVARCHAR(255),StudentID INT,TeacherID INT,CreditUnits INT)")

c.execute("INSERT INTO Students (StudentName,ParentName, City,Country  , GPA, dateStarted) VALUES ('Tanya Anand', 'Amit Anand','Delhi','India',3.31,'2015-01-12')");
c.execute("INSERT INTO Students (StudentName,ParentName, City,Country  , GPA ,  dateStarted) VALUES ('Ishita Reddy','Aditya Reddy','Bangalore','India',3.12,'2013-10-23')");
c.execute("INSERT INTO Students (StudentName,ParentName, City,Country  , GPA , dateStarted) VALUES ('Ankit Babu','Raj Babu','Banerjee','India',2.73,'2015-04-05')");
c.execute("INSERT INTO Students (StudentName,ParentName, City ,Country  , GPA ,  dateStarted) VALUES ('Suhani Burman','Anusha Burman','Bangalore','India',2.57,'2014-10-22')");

c.execute("select * from students")

for record in c.fetchall():
    print(record)

conn.commit()
c.close()
print("These tables have been created successfully")
