import sqlite3

# Setting up database, Creating a new one or going to an existing one
connect = sqlite3.connect('TeacherStudent.db')
c = connect.cursor()
id = 2342
room = 2
first = 'Sid'
last = 'Anand'
grade = 9

newGrade = 10
teacher_id = 4002

# creating a table if one does not exist
# c.execute("CREATE table users( id INTEGER PRIMAry KEY, name TEXT, phone TEXT)")

# deletes a table
# c.execute (DROP TABLE users)

# Executing terminal commands in python
# Question marks are place holders for the unknown value
# single entry
c.execute('INSERT INTO teacher(teacher_id, first_name, last_name, grade, room_number)VALUES(?, ?, ?, ?, ?)', (id, first, last, grade, room))

# Updating the code
c.execute('UPDATE teacher SET grade = ? WHERE teacher_id = ?', (newGrade, teacher_id))

# This should always be last
# reading the table
c.execute('SELECT * FROM  teacher')

# Reading a specific entry - an entire row
# The star can be changed with specific location
c.execute('SELECT * FROM  teacher WHERE teacher_id')

# This just undo button
# conn.rollback()

# This pulls the specific data from the table
# the [0] can be taken out. This will cause it to print the entire results(columns) as a list
# c.execute('SELECT name, email, phone FROM users')
# user1=c.fetchone()
# print(user1[0])

# Fetching the data and printing it out
# Fetchall() goes through the data
rows = c.fetchall()
for r in rows:
    print(r)

# This is an alternate method for reading without having to create another variable
# c.execute('SELECT name, email, phone FROM users')
# for r in c:
#    print(r)

# Creates a table if it does not exist
# c.execute('CREATE TABLE IF NOT EXISTS \
#   users(id INTEGER PRIMARY KEY, name TEXT, phone TEXT, email TEXT unique, password TEXT)')

# Saves the database
connect.commit()
c.close()