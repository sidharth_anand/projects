from tkinter import *
import sqlite3

root = Tk()
root.title("Python: Simple Login Application")
width = 400
height = 350
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width/2) - (width/2)
y = (screen_height/2) - (height/2)
root.geometry("%dx%d+%d+%d" % (width, height, x, y))
root.resizable(0, 0)

USERNAME = ''
PASSWORD = ''

conn = sqlite3.connect("pythontut.db")
cursor = conn.cursor()
# cursor.execute('DELETE FROM mem WHERE id=?')
cursor.execute("CREATE TABLE IF NOT EXISTS `member` (mem_id INTEGER NOT NULL PRIMARY KEY  AUTOINCREMENT, username TEXT,"
               " password TEXT)")
cursor.execute("SELECT * FROM `member` WHERE `username` = 'admin' AND `password` = 'admin'")

def Database():

    if cursor.fetchone() is None:
        cursor.execute("INSERT INTO `member` (username, password) VALUES('admin', 'admin')")

def Login(event=None):
    Database()


    if USERNAME.get() == "" or PASSWORD.get() == "":
        lbl_text.config(text="Please complete the required field!", fg="red")
    else:
        cursor.execute("SELECT * FROM `member` WHERE `username` = ? AND `password` = ?", (USERNAME.get(),
                                                                                          PASSWORD.get()))
        if cursor.fetchone() is not None:
            HomeWindow()
            USERNAME.set("")
            PASSWORD.set("")
            lbl_text.config(text="")
        else:
            lbl_text.config(text="Invalid username or password", fg="red")
            USERNAME.set("")
            PASSWORD.set("")

    username.delete(0, END)
    password.delete(0, END)

    #cursor.close()
    #conn.close()

def HomeWindow():
    global Home
    root.withdraw()
    Home = Toplevel()
    Home.title("Python: Simple Login Application")
    width = 600
    height = 500
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width/2) - (width/2)
    y = (screen_height/2) - (height/2)

    root.resizable(0, 0)
    Home.geometry("%dx%d+%d+%d" % (width, height, x, y))
    lbl_home = Label(Home, text="Successfully Login!", font=('times new roman', 20)).pack()
    btn_back = Button(Home, text='Back', command=Back).pack(pady=20, fill=X)

def Back():
    Home.destroy()
    root.deiconify()

def SignUpWindow():
    global SignUp
    global USERNAME
    global PASSWORD

    root.withdraw()
    SignUp = Toplevel()
    #SignUp.title("Python: Simple Login Application")

    width = 600
    height = 500
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    x = (screen_width / 2) - (width / 2)
    y = (screen_height / 2) - (height / 2)
    root.resizable(0, 0)

    #SignUp.geometry("%dx%d+%d+%d" % (width, height, x, y))

    #lbl_title_SU = Label(, text="Python: Simple Login Application", font=('arial', 15))
    #lbl_title_SU.pack(fill=X)

    lbl_username_SU = Label(SignUp, text="Username:", font=('arial', 14), bd=15)
    lbl_username_SU.grid(row=0, sticky="e")

    lbl_password_SU = Label(SignUp, text="Password:", font=('arial', 14), bd=15)
    lbl_password_SU.grid(row=1, sticky="e")

    lbl_text_SU = Label(SignUp)
    lbl_text_SU.grid(row=2, columnspan=2)

    username_SU = Entry(SignUp, textvariable=USERNAME, font=14)
    username_SU.grid(row=0, column=1)

    password_SU = Entry(SignUp, textvariable=PASSWORD, show="*", font=(14))
    password_SU.grid(row=1, column=1)

    btn_login_SU = Button(SignUp, text="Sign Up", width=45, command=SignUpSuccessWindow)
    btn_login_SU.grid(pady=25, row=3, columnspan=2)
#    btn_login_SU.bind('<Return>', SignUpSuccessWindow)

    #SignUpUser = Label(SignUp, text="Enter your username", font=('times new roman', 20)).grid
    #lbl_home = Label(SignUp, text="!", font=('times new roman', 20)).pack()
    #btn_back = Button(SignUp, text='Back', command=Back).pack(pady=20, fill=X)

def SignUpSuccessWindow():
    SignUp.destroy()

    global SignUpSuccess
    root.withdraw()
    SignUpSuccess = Toplevel()
    SignUpSuccess.title("Sign Up Success Window")
    width = 600
    height = 500
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width / 2) - (width / 2)
    y = (screen_height / 2) - (height / 2)
    root.resizable(0, 0)
    SignUpSuccess.geometry("%dx%d+%d+%d" % (width, height, x, y))
    lbl_home_SUS = Label(SignUpSuccess, text="Successfully Signed Up!", font=('times new roman', 20)).pack()
    btn_back_SUS = Button(SignUpSuccess, text='Back', command=BackSUS).pack(pady=20, fill=X)

    print(USERNAME.get())
    print(PASSWORD.get())

    cursor.execute('INSERT INTO member(username, password) VALUES(?, ?)', (USERNAME.get(), PASSWORD.get()))


def BackSUS():
    username.delete(0, END)
    password.delete(0, END)
    SignUpSuccess.destroy()
    root.deiconify()


USERNAME = StringVar()
PASSWORD = StringVar()

Top = Frame(root, bd=2,  relief=RIDGE)
Top.pack(side=TOP, fill=X)
Form = Frame(root, height=200)
Form.pack(side=TOP, pady=20)


lbl_title = Label(Top, text = "Python: Simple Login Application", font=('arial', 15))
lbl_title.pack(fill=X)
lbl_username = Label(Form, text = "Username:", font=('arial', 14), bd=15)
lbl_username.grid(row=0, sticky="e")
lbl_password = Label(Form, text = "Password:", font=('arial', 14), bd=15)
lbl_password.grid(row=1, sticky="e")
lbl_text = Label(Form)
lbl_text.grid(row=2, columnspan=2)


username = Entry(Form, textvariable=USERNAME, font=(14))
username.grid(row=0, column=1)
password = Entry(Form, textvariable=PASSWORD, show="*", font=(14))
password.grid(row=1, column=1)


btn_login = Button(Form, text="Login", width=45, command=Login)
btn_login.grid(pady=25, row=3, columnspan=2)
btn_login.bind('<Return>', Login)

btn_sign_up = Button(Form, text="Sign Up", width=45, command=SignUpWindow)
btn_sign_up.grid(row=4, columnspan=2)
btn_sign_up.bind('<Tab>', SignUpWindow)

root.mainloop()

cursor.close()
conn.close()
conn.commit()