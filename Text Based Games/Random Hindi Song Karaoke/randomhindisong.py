'''
import random 
from pygame import mixer
import time

# lists
word = ['pyaar', 'dil', 'deewana', 'ishq', 'tanha', 'hum', 'saath',
        'mere', 'ma', 'koi', 'kahe', 'kehta', 'rahe']

# shuffles contents in list
random.shuffle(word)

# sets up music
mixer.init()
mixer.music.load('randombollywoodmusic.mp3')
mixer.music.play()

# for x in word, the code prints the song 
for x in word:
    print (x)

# stops the music after 100 seconds 
time.sleep(100)
mixer.music.stop()
'''

import random 
from pygame import mixer
import time

# lists
word = ['pyaar', 'dil', 'deewana', 'ishq', 'tanha', 'hum', 'saath',
        'mere', 'ma', 'koi', 'kahe', 'kehta', 'rahe']
wordhindi = ['प्यार', 'दिल', 'दीवाना', 'इश्क़', 'तनहा', 'हम', 'साथ',
        'मेरे', 'माँ', 'कोई', 'कहे', 'कहता', 'रहे']
songs = ['song1.mp3', 'song2.mp3', 'song3.mp3', 'song4.mp3', 'song5.mp3']

# shuffles contents in list
random.shuffle(word)
random.shuffle(songs)

mixer.init()

print('Hi there, you have decided to do random bollywood song challenge!')
print('You get a chance to choose a random song and words will be random')
print('Long is 60 seconds, Medium is 40 seconds, and Short is 25 seconds')
print('Words will be printed out all at once so sing at your own rate')

time.sleep(2)

print('Would you like to do the random bollywood karaoke challenge?')
challenge = input()

print('How long would you like to sing for? short, medium, or long')
singlength = input()

print('Would you like the words in Hindi')
hindi = input()

if hindi == 'no':
    
    if challenge.lower() == 'yes':
        print('You will now pick a random song among the four options')
        print('Pick a number between 0 ', len(songs)-1, ' including 0')
        songnum = int(input())  

        mixer.music.load(songs[songnum])
        mixer.music.play()

        if singlength.lower() == 'short':
            for x in range(25):
                wordrand = random.randint(0,12)
                print(word[wordrand])
            time.sleep(20)
            mixer.music.stop()

        if singlength.lower() == 'medium':
            for x in range(25):
                wordrand = random.randint(0,12)
                print(word[wordrand])
            time.sleep(40)
            mixer.music.stop()

        if singlength.lower() == 'long':
            for x in range(25):
                wordrand = random.randint(0,12)
                print(word[wordrand])

    else:
        print('Thanking you for stopping bye')

else:
        
    if challenge.lower() == 'yes':
        print('You will now pick a random song among the four options')
        print('Pick a number between 0 ', len(songs)-1, ' including 0')
        songnum = int(input())  

        mixer.music.load(songs[songnum])
        mixer.music.play()

        if singlength.lower() == 'short':
            for x in range(25):
                wordrand = random.randint(0,12)
                print(wordhindi[wordrand])
            time.sleep(20)
            mixer.music.stop()

        if singlength.lower() == 'medium':
            for x in range(25):
                wordrand = random.randint(0,12)
                print(wordhindi[wordrand])
            time.sleep(40)
            mixer.music.stop()

        if singlength.lower() == 'long':
            for x in range(25):
                wordrand = random.randint(0,12)
                print(wordhindi[wordrand])

    else:
        print('Thanking you for stopping bye')
        
time.sleep(60)
mixer.music.stop()
print('Come again')
        
        

           

    

    
