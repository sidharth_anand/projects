from flask import Flask, render_template

app = Flask("Sid's Website")

@app.route("/")
def index():
    return render_template("index.html")

if "__main__" == __name__:
    app.run(debug=True)





