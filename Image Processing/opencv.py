import cv2
import numpy

img1 = cv2.imread("images/image1.png")
img2 = cv2.imread("images/image2.jpg")

i1 = cv2.resize(img1, (150, 250))
i2 = cv2.resize(img2, (150, 250))

img = cv2.addWeighted(i1, 0.5, i2, 0.5, 0.0)
# print(img)

cv2.imshow("Test", img)
cv2.waitKey()
cv2.destroyAllWindows()


