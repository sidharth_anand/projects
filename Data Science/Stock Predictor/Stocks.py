import pandas_datareader as pdr
import pandas as pd
import matplotlib.pyplot as plt
from tkinter import *
import os
import datetime

root = Tk()

def method():
    one = compOneEntry.get()
    two = compTwoEntry.get()

    oneStartDay = oneStartDayEntry.get()
    oneStartMonth = oneStartMonthEntry.get()
    oneStartYear = oneStartYearEntry.get()
    oneEndDay = oneEndDayEntry.get()
    oneEndMonth = oneEndMonthEntry.get()
    oneEndYear = oneEndYearEntry.get()

    oneStartDate = datetime.datetime(int(oneStartYear), int(oneStartMonth), int(oneStartDay))
    oneEndDate = datetime.datetime(int(oneEndYear), int(oneEndMonth), int(oneEndDay))


    twoStartDay = twoStartDayEntry.get()
    twoStartMonth = twoStartMonthEntry.get()
    twoStartYear = twoStartYearEntry.get()
    twoEndDay = twoEndDayEntry.get()
    twoEndMonth = twoEndMonthEntry.get()
    twoEndYear = twoEndYearEntry.get()

    twoStartDate = datetime.datetime(int(twoStartYear), int(twoStartMonth), int(twoStartDay))
    twoEndDate = datetime.datetime(int(twoEndYear), int(twoEndMonth), int(twoEndDay))

    oneList = pdr.get_data_yahoo(one, start=oneStartDate, end=oneEndDate)
    twoList = pdr.get_data_yahoo(two, start=twoStartDate, end=twoEndDate)

    oneList.to_csv(one+".csv")
    twoList.to_csv(two+".csv")

    compOneList = pd.read_csv(one+".csv")
    compTwoList = pd.read_csv(two+".csv")

    plt.plot(compOneList["Date"], compOneList["Adj Close"])
    plt.plot(compTwoList["Date"], compTwoList["Adj Close"])
    plt.show()

    os.remove(one+".csv")
    os.remove(two+".csv")


compOne = Label(root, text='Company 1')
compTwo = Label(root, text='Company 2')

oneStartDay = Label(root, text='Start Day')
oneEndDay = Label(root, text='End Day')

oneStartMonth = Label(root, text='Start Month')
oneEndMonth = Label(root, text='End Month')

oneStartYear = Label(root, text='Start Year')
oneEndYear = Label(root, text='End Year')

compOneEntry = Entry(root)
compTwoEntry = Entry(root)
oneStartYearEntry = Entry(root)
oneStartMonthEntry = Entry(root)
oneStartDayEntry = Entry(root)
oneEndDayEntry = Entry(root)
oneEndMonthEntry = Entry(root)
oneEndYearEntry = Entry(root)

twoStartYearEntry = Entry(root)
twoStartMonthEntry = Entry(root)
twoStartDayEntry = Entry(root)
twoEndDayEntry = Entry(root)
twoEndMonthEntry = Entry(root)
twoEndYearEntry = Entry(root)

submit = Button(root, text='submit', command=method)

twoStartDayEntry.grid(row=3, column=3)
twoStartMonthEntry.grid(row=3, column=4)
twoStartYearEntry.grid(row=3, column=5)
twoEndDayEntry.grid(row=3, column=6)
twoEndMonthEntry.grid(row=3, column=7)
twoEndYearEntry.grid(row=3, column=8)

compOne.grid(row=2, column=0)
compTwo.grid(row=3, column=0)

compOneEntry.grid(row=2, column=1)
compTwoEntry.grid(row=3, column=1)

oneStartDayEntry.grid(row=2, column=3)
oneStartDay.grid(row=1, column=3)
oneStartMonthEntry.grid(row=2, column=4)
oneStartMonth.grid(row=1, column=4)
oneStartYearEntry.grid(row=2, column=5)
oneStartYear.grid(row=1, column=5)

oneEndDay.grid(row=1, column=6)
oneEndDayEntry.grid(row=2, column=6)
oneEndMonth.grid(row=1, column=7)
oneEndMonthEntry.grid(row=2, column=7)

oneEndYear.grid(row=1, column=8)
oneEndYearEntry.grid(row=2, column=8)

submit.grid(row=4, column=0)

root.mainloop()

"""
google = pd.read_csv("google.csv")
amazon = pd.read_csv("amazon.csv")

plt.plot(amazon["Date"][0:100:], amazon["Adj Close"][0:100:])
plt.plot(google["Date"][0:100:], google["Adj Close"][0:100:])
plt.show()

print("How many company's stocks would you like to compare")
reps = int(input())

for x in range(reps):
    print("Enter company number ", x)
    company = input()
"""

