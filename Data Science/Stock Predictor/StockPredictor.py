import pandas_datareader as pdr
import matplotlib.pyplot as plt
import datetime
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import numpy

google = pdr.get_data_yahoo("GOOGL", start=datetime.datetime(2000, 1, 1), end=datetime.datetime(2019, 1, 1))

# print(google.index)
google["Date"] = google.index.to_julian_date()
x = google["Date"]
y = google['Adj Close']

xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.33, random_state=24)

xtrain = numpy.array(xtrain).reshape(-1, 1)
ytrain = numpy.array(ytrain).reshape(-1, 1)
xtest = numpy.array(xtest).reshape(-1, 1)
ytest = numpy.array(ytest).reshape(-1, 1)

lr = LinearRegression(normalize=True)
lr.fit(xtrain, ytrain)
print(lr.score(xtest, ytest))

# p = []
# for x in xtrain:
#     p.append(datetime.datetime.fromtimestamp(x))
#
# print(p)

ypred = lr.predict(xtest)
plt.plot(xtest, ypred)
plt.scatter(xtrain, ytrain)

plt.show()


