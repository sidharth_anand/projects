import matplotlib.pyplot as plt

constitutionFile = open("constitution.txt", "r")
constitution = constitutionFile.read()
wordList = constitution.split()
occurences = {}

for x in wordList:
    x = x.strip()
    if len(x) >= 5:
        if x not in occurences:
            occurences[x] = 1
        else:
            occurences[x] += 1

listOccur = list(occurences.values())
listOccur.sort()
listOccur.reverse()
topWords = []
wordCount = []

for x in listOccur[0:10]:
    for y in occurences:
        if x == occurences[y]:
            wordCount.append(occurences[y])
            topWords.append(y)

print(len(wordCount))
print(topWords)

plt.bar(topWords, wordCount, label = "Most Frequent Occuring Word Over 5 Letters")
plt.show()








