from twitter import *
import matplotlib.pyplot as plt
import numpy as np
import time


twitter = Twitter(
        auth = OAuth("779453170669936640-EPQ1zemuJAAw2VDEN3TzdyU84ly09oy", "clRLH7s3NqrJtbOlq4eqVAWt4tP9lkHjM7SQ3EhV5o1Dr", "5TaeQY8XcU3ejmuyYomICK6jr", "m4Fasndg6XILDpGYjYDDbaHP9UvryXQDJUe76Te1fHGi3feuj4"))

twitter_username = input("Twitter user? ")

def getFriends(twitter_username):
    friend_ids = twitter.friends.ids(screen_name=twitter_username)

    friends = []
    for i in range(0, len(friend_ids["ids"]), 100):
      hundred_ids = friend_ids["ids"][i:i+100]
      users = twitter.users.lookup(user_id = hundred_ids)
      for user in users:
          friends.append(user["screen_name"])
      time.sleep(1)
    time.sleep(3)
    return friends

bar_names = getFriends(twitter_username)
bar_indexes = np.arange(len(bar_names))
bar_heights = [0] * len(bar_names)

for i, bar_name in enumerate(bar_names):
    print(bar_name)
    bar_heights[i] = len(getFriends(bar_name))

print(bar_heights)