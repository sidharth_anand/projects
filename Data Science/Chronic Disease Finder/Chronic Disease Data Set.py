import matplotlib.pyplot as plt
import csv

"""
Write a program to calculate number of people affected by each of the diseases.

Use a pie chart to represent the share of each disease.

Find the number of people affected by Alcohol with a confidence level difference of more than 3.

Find the number of people affected by alcohol in Idaho.

Find the number of Males affected by Arthritis.

"""

file = open("U.S._Chronic_Disease_Indicators__CDI_.csv", "r")
plot = csv.reader(file, delimiter = ",")

# 15 and 16
frequency = {}
disease = "Arthritis"
gender = "Male"
counter = 0

for x in plot:
    if x[5] == "Topic":
        continue

    if x[5] == disease:
        try:
            if gender in x:
                counter += 1
        except:
            continue

print(counter)


"""
for x in plot:
    if x[5] == "Topic":
        continue
    if x[5] == "Alcohol":
        try:
            confidence = float(x[15]) - float(x[14])
            if confidence >= 3:
                counter += 1
        except:
            print("Invalid Data ", x[15], " ", x[14])

print(counter)
"""
"""
# run through the file and add dieasease and diease occurance to the dicitonary
for x in plot:
    if x[5] == "Topic":
        continue
    if x[5] not in frequency:
        frequency[x[5]] = 1
    else:
        frequency[x[5]] += 1

    # if counter == 20000:
    #     break
    # counter += 1
print(frequency)

# plt.bar(frequency.keys(),frequency.values())
# plt.show()
"""
